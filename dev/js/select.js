"use strict";
jQuery(function($) {
    var sel = {
        init: function() {
            this.init_cache();
            this.events();
            this.init_selects();
        },

        init_cache: function() {
            this.$module = $(".js-select-module");
            this.$select = $(".js-select-module-select");
            this.$container = $(".js-select-module-container");
            this.$opt_container = $(".js-select-module-options");
            this.$btn = $(".js-select-module-text-block");
        },

        events: function() {
            this.$btn.click(function() {
                $(this)
                    .closest(sel.$module)
                    .toggleClass("_active");
            });
        },

        init_selects: function() {
            this.$module.each(function() {
                var $item = $(this),
                    $select = $item.find(sel.$select),
                    $container = $item.find(sel.$container),
                    input = sel.construct_input($select),
                    $opt_container = $item.find(sel.$opt_container),
                    $btn = $item.find(sel.$btn);

                sel.build_options($select, $opt_container, $btn);
                $container.append(input);
                sel.add_events($item, $btn);
            });
        },
        construct_input: function($select) {
            var name = $select.data("name"),
                value = $select.val(),
                input =
                    '<input type="hidden" class="select-module__input js-select-module-input pseudo-hidden" name="' +
                    name +
                    '" value="' +
                    value +
                    '"/>';

            return input;
        },

        build_options: function($select, $opt_container, $btn) {
            var $options = $select.find("option"),
                $select_value = $select.val();

            for (var i = 0; i < $options.length; i++) {
                var $item = $($options[i]),
                    value = $item.val(),
                    title = $item.text();

                $opt_container.append(
                    '<div class="select-module__option js-select-module-option ' +
                        ($select_value === value ? "_active" : "") +
                        '" data-value="' +
                        value +
                        '">' +
                        title +
                        "</div>"
                );

                if ($select_value === value) {
                    //$btn.text(title);
                }
            }

            $opt_container.perfectScrollbar();
        },

        add_events: function($item, $btn) {
            var $input = $item.find(".js-select-module-input"),
                $options = $item.find(".js-select-module-option");

            $options.on("click", function() {
                var $opt = $(this),
                    value = $opt.data("value"),
                    title = $opt.text(),
                    $form = $options.closest("form");

                $options.add($item).removeClass("_active");
                $opt.addClass("_active");

                $btn.text(title);
                $input.val(value);
                $form.change();
            });
        }
    };

    $(window).load(function() {
        sel.init();
    });
});
