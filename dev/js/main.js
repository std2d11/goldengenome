require('jquery');

require('./plugin/jquery.inputmask.bundle.min');
require('./plugin/jquery.validate.min');
require('./plugin/perfect-scrollbar.jquery.min');
require('./plugin/slick');
// require('./plugin/numscroller-1.0');

require('./popups.js');
require('./select.js');
require('./video.js');
require('./particles.js');

require('./plugin/photobox.js');
// require('./plugin/hc-sticky.js');


require('./forms.js');
require('./sliders.js');
require('./script.js');
