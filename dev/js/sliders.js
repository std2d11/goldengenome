'use strict'
jQuery(function($) {

    var _sl = {
        init: function() {
            this.init_cache();
            this.reviews();


        },

        init_cache: function() {
            this.$reviewsSlider = $('.js-reviews-slider');


            this.$menuSlider = $('.js-menu-slider');
            this.$menuItem = $('.js-menu-item');
            this.$menuNext = $('.js-menu-nav-next');
            this.$menuPrev = $('.js-menu-nav-prev');
            this.$menuContent = $('.js-menu-content');
            this.$reasonSlider = $('.js-reason-slider');
            this.$familySlider = $('.js-family-slider');
        },



        reviews: function() {
            _sl.$reviewsSlider.slick({
                dots: false,
                arrows: true,
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [{
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 700,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false,
                            dots: true
                        }
                    }
                ]
            });
        },














        familySlider: function() {
            _sl.$familySlider.slick({
                dots: false,
                arrows: true,
                infinite: true,
                speed: 500,
                fade: true,
                cssEase: 'linear'
            });
        },


        reasonsSlider: function() {
            _sl.$reasonSlider.slick({
                dots: false,
                arrows: true,
                infinite: true,
                speed: 500,
                fade: true,
                cssEase: 'linear'
            });
        },

        menuSlider: function() {

            _sl.$menuContent.each(function() {
                var $items = $(this).find(_sl.$menuItem),
                    $slider = $(this).find(_sl.$menuSlider);

                $items.eq(0).addClass('_active');

                $items.click(function() {
                    $items.removeClass('_active');
                    $(this).addClass('_active');

                    $slider.slick('slickGoTo', $(this).index())
                });

                $slider
                    .on('afterChange', function(event, slick, currentSlide, nextSlide) {
                        $items.removeClass('_active');
                        $items.eq(currentSlide).addClass('_active');
                    })
                    .slick({
                        dots: false,
                        arrows: true,
                        infinite: false,
                        speed: 500,
                        fade: true,
                        cssEase: 'linear'
                    });
            });


            _sl.$menuMainSlider.slick({
                draggable: false,
                dots: false,
                arrows: false,
                infinite: false,
                speed: 500,
                fade: true,
                cssEase: 'linear'
            });


            _sl.$menuPrev.click(function() {
                _sl.$menuMainSlider.slick('slickPrev');
            });

            _sl.$menuNext.click(function() {
                _sl.$menuMainSlider.slick('slickNext');
            });
        },



        objectSlider: function() {
            _sl.$object
                .on('init', function(event, slick) {
                    _sl.initCounter($(this));
                })
                .on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                    _sl.initCounter($(this), nextSlide);
                })
                .slick({
                    infinite: false,
                    arrows: true,
                    dots: false,
                    slidesToShow: 2,
                    slidesToScroll: 1
                })


        },


        bottomProductSlider: function() {
            _sl.$productBottom.slick({
                infinite: true,
                arrows: true,
                dots: false,
                slidesToShow: 2,
                slidesToScroll: 1
            });
        },

        mainProductSlider: function() {
            _sl.$productMain
                .on('init', function(event, slick) {
                    _sl.initCounter($(this));
                })
                .on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                    _sl.initCounter($(this), nextSlide);
                })
                .slick({
                    dots: false,
                    arrows: true,
                    infinite: false,
                    speed: 500,
                    fade: true,
                    cssEase: 'linear'
                });
        },

        homeSlider: function() {
            _sl.$homeSlider.slick({
                infinite: false,
                arrows: false,
                dots: true,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        },

        optionSliders: function() {
            _sl.$optionMain.slick({
                dots: false,
                arrows: false,
                infinite: false,
                speed: 500,
                fade: true,
                cssEase: 'linear',
                draggable: false
            });


            _sl.$optionMainNav.click(function() {
                _sl.$optionMainNav.removeClass('_active');
                $(this).addClass('_active');
                _sl.$optionMain.slick('slickGoTo', $(this).index());

                // _sl.initCounter('bla');
            });

            //-------------------------------------

            _sl.$optionInner
                .on('init', function(event, slick) {
                    _sl.initCounter($(this));
                })
                .on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                    _sl.initCounter($(this), nextSlide);
                })
                .slick({
                    dots: false,
                    arrows: true,
                    infinite: false,
                    speed: 500,
                    fade: true,
                    cssEase: 'linear'
                });

            //-------------------------------------

            _sl.$optionSmallTabSlider.slick({
                dots: false,
                arrows: false,
                infinite: false,
                speed: 500,
                fade: true,
                cssEase: 'linear',
                draggable: false
            });


            _sl.$optionSmallTabNav.click(function() {
                _sl.$optionSmallTabNav.removeClass('_active');
                $(this).addClass('_active');
                _sl.$optionSmallTabSlider.slick('slickGoTo', $(this).index());
            });
        },

        stagesSlider: function() {
            _sl.$stagesSlider
                .on('init', function(event, slick) {
                    _sl.initCounter($(this));
                })
                .on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                    _sl.initCounter($(this), nextSlide);
                })
                .slick({
                    dots: false,
                    arrows: true,
                    infinite: false,
                    speed: 500,
                    fade: true,
                    cssEase: 'linear'
                });
        }


    };

    $(document).ready(function() {
        _sl.init();
    });
});