//using plugins:
//jquery v2.1.4
//jquery.validate
//jquery.inputmask Version: 4.0.0-beta.18

'use strict'
jQuery(function($) {

    window.form_adjuster = {
        init: function(options) {
            this.options = options;
            this.init_cache();
            this.init_validation();
            this.init_mask();
            this.btn_reset_init();

            if (this.options['file']) {
                //this.build_file_structure();
                this.check_file();
                this.input_file_reset();
            }

            this.send_validation();
            this.init_btn_no_ajax();
            this.init_ajax_without_form();
            this.number_only_init();
        },

        init_options: function() {
            var default_handler_url = window.wp_data ? window.wp_data : false,
                default_settings = {
                    'type': 'POST',
                    // 'handler': default_handler_url.sau_sender_ajax_url ? default_handler_url.sau_sender_ajax_url : './sender.php',
                    'handler': './sender.php',
                    'dataType': 'json',
                    'contentType': false,
                    'processData': false,
                    'task':'task',

                    //file
                    'file': false, //not finished yet
                    'max_weight':8388608,
                    //callbacks for file
                    'onload': false,
                    //classes for input type file
                    //.js-file-preview - for preview

                    //callbacks success,error for ajax
                    'success': false,
                    'error': false
                };
            return this.options ? $.extend(default_settings, this.options) : default_settings
        },

        init_cache: function(options) {
            this.options = this.init_options();
            this.$input_phone = $('.userphone');
            this.$input_date = $('.js-input-date');
            this.$form = $('form:not(.js-no-ajax)');
            this.$noajax_btn = $('.js-noajax-btn');
            this.$number_only = $('.js-number-only');
            this.$btn_form_reset = $('.js-form-reset');

            //file
            this.$file_input = $('.js-file-check');

            //ajax without form
            //parameters: data-json="{'action':'foo','parameters':'bar'}"
            this.$btn_without_form = $('.js-single-button');
        },

        init_validation: function() {
            $.validator.addMethod(
                'regexp',
                function(value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },
                "Please check your input."
            );


            $.validator.addClassRules({
                userphone: {
                    minlength: 15,
                    regexp: '[^_]+$'
                },
                usermail: {
                    email: true,
                    required: true
                },
                required: {
                    required: true
                },
                password:{
                    minlength:6
                },
                passwordConfirm:{
                    minlength:6,
                    equalTo:'.js-input-new-password'
                }
            });

        },

        init_mask: function() {
            this.$input_phone.inputmask({ "mask": "+7(999)999-99-99",showMaskOnHover: false });
            this.$input_date.inputmask("dd/mm/yyyy",{showMaskOnHover: false });
        },

        //send form with custom ajax
        form_send: function(formObject, action) {
            var settings = form_adjuster.options;

            //formObject - formData,
            //action - action for handler
            //form_adjuster.$form_cur - current form
            $.ajax({
                type: settings['type'],
                url: settings['handler'],
                //full url in global variable wp_data.sau_sender_ajax_url
                // + "?action=" + (action ? action : "sau_send_mail"),
                dataType: settings['dataType'],
                contentType: settings['contentType'],
                processData: settings['processData'],
                data: formObject,
                success: function() {
                    if (settings['success']) {
                        settings['success']();

                    } else {
                        //form_adjuster.ajax_success();
                    }

                    form_adjuster.$form_cur = false;
                },
                error: function() {
                  settings['success']();

                  form_adjuster.ajax_success();
                    if (settings['error']) {
                        //settings['error']();


                    } else {
                      //  form_adjuster.ajax_error();
                    }

                    form_adjuster.$form_cur = false;
                }
            });
        },

        ajax_success: function() {
            console.log('success');

            if (form_adjuster.$form_cur) {
                form_adjuster.$form_cur.trigger('reset');
                //doing smth
            }
        },

        ajax_error: function() {
            console.log('error');
        },

        //assembly form with formData
        formData_assembly: function(form) {
            var formSendAll = new FormData(),
                formdata = {},
                form_arr,
                $form = $(form),
                $fields = $form.find(':input,select,textarea'),
                pos_arr = [];

            form_arr = $fields.serializeArray();

            for (var i = 0; i < form_arr.length; i++) {
                if (form_arr[i].value.length > 0) {

                    var $current_input = $fields.filter('[name=' + form_arr[i].name + ']'),
                        value_arr = {};

                    if ($current_input.attr('type') != 'hidden' && !$current_input.hasClass('js-system-field')) {
                        var title = $current_input.attr('data-title');

                        value_arr['value'] = form_arr[i].value;
                        value_arr['title'] = title;
                        formdata[form_arr[i].name] = value_arr;

                    } else {
                        if(!$current_input.hasClass('js-system-field')){
                            formSendAll.append(form_arr[i].name, form_arr[i].value);
                        }
                    }
                }
            }

            if (pos_arr.length > 0){
                formdata['position'] = pos_arr;
            }

            formSendAll.append('formData', JSON.stringify(formdata));

            //file
            if (form_adjuster.options['file']) {
                var $input_file = $(form).find('.js-file-check');

                if ($input_file.length > 0) {
                    $input_file.each(function() {
                        var $input_cur = $(this),
                            val_length = $input_cur.val().length,
                            multy = $input_cur.prop('multiple');

                        if (val_length > 0) {
                            if (!multy) {
                                formSendAll.append($input_cur.attr('name'), $input_cur[0].files[0]);
                            } else {
                                form_adjuster.collect_multiple_file(formSendAll, $input_cur);
                            }
                        }
                    })
                }
            }

            this.form_send(formSendAll, false);
        },

        collect_multiple_file: function(data, $input) {
            var $wrapper = $input.closest('.js-file-module'),
                $list = $wrapper.find('.js-file-list');

            $('.js-file-list li').each(function() {
                var file_name = $(this).attr('data-name');

                for (var i = 0; i < $input[0].files.length; i++) {

                    if (file_name == $input[0].files[i].name) {

                        data.append($input.attr('name'), $input[0].files[i]);
                    }
                }
            })
        },

        //file reader and handlers
        check_file: function() {
            var reader;

            function abortRead() {
                reader.abort();
            }

            function errorHandler(evt) {
                switch (evt.target.error.code) {
                    case evt.target.error.NOT_FOUND_ERR:
                        alert('File Not Found!');
                        break;
                    case evt.target.error.NOT_READABLE_ERR:
                        alert('File is not readable');
                        break;
                    case evt.target.error.ABORT_ERR:
                        break; // noop
                    default:
                        alert('An error occurred reading this file.');
                };
            }

            function handleFileSelect(evt) {
                var $input = $(this);

                for (var i = 0; i < $input[0].files.length; i++) {
                    reader_file($input[0].files[i], $input);
                }
            }

            function reader_file(file, $input) {
                var reader = new FileReader(),
                    file_name = file.name;

                reader.file_name = file_name;
                reader.onerror = errorHandler;

                reader.onabort = function(e) {
                    alert('File read cancelled');
                };

                reader.onload = function(event) {
                    if (form_adjuster.options['onload']) {
                        form_adjuster.options['onload']();
                    } else {
                        if($input[0].files[0].size * 8 < form_adjuster.options.max_weight){
                            form_adjuster.file_onload($input,reader);
                        }
                        else{
                            alert('Файл больше 2 мб.');
                        }
                    }
                };

                reader.onprogress = function(event) {
                    //progress bar
                    // if (event.lengthComputable) {
                    //     var percent = parseInt(((event.loaded / event.total) * 100), 10),
                    //         $bar = $wrapper.find('.js-upload-progressbar');

                    //     $bar.css('width', percent + '%');
                    // }
                };

                reader.readAsDataURL(file);
            }

            form_adjuster.$file_input.on('change', handleFileSelect);
        },

        file_onload: function($input,reader) {
            var $module = $input.closest('.js-file-module'),
                $list = $module.find('.js-file-preview-list'),
                $input_block = $module.find('.js-file-input-list');

            if(!$module.hasClass('_onload')){
                $module.addClass('_onload');
            }

            // $list.append('<div class="object-fit-wrap" data-name="' +
            //     reader.file_name + '"><div class="object-fit-wrap__img-wrapper"><img class="object-fit-wrap__img js-file-preview" src="' +
            //     reader.result + '"/></div><i class="file-onload-block__icon-reset fa fa-times js-file-reset"></i></div>');

            $list.append('<div class="new-file-wrap" data-name="'+  reader.file_name + '"><i class="file-remove fa fa-times js-file-reset"></i><div class="file-name"> ' +  reader.file_name  + '</div></div>')

            var $img = $module.find('js-file-preview'),
                $previews = $module.find('.object-fit-wrap');

            // $.fn.objectFitImages($img);

            $input_block.append($input.clone(true).removeClass('js-file-start').val(''));

            if($previews.length > 5){
                $module.addClass('_full');
            }

        },

        build_file_structure: function() {
            form_adjuster.$file_input.each(function() {
                var $input = $(this),
                    params = $input.data('params');

                params = params ? params : {};

                $input.wrap('<div class="upload-wrapper js-upload-wrapper' +
                    (params.mod ? " upload-wrapper_" + params.mod : "") + '"></div>')
                    .after('<span class="upload-wrapper__label"><span class="upload-wrapper__text js-upload-text">' +
                        (params.label ? params.label : 'Изменить') + '</span>' +
                        (params.mod === 'simple' ? '<span class="cross-btn js-file-reset"></span>' : '') + '</span>');
            })
        },

        //reset input type file with save all handlers
        input_file_reset: function() {
            $(document).on('click', '.js-file-reset', function() {
                console.log('click')
                var $btn = $(this),
                    $item = $btn.closest('.new-file-wrap'),
                    name = $item.data('name'),
                    $module = $item.closest('.js-file-module'),
                    $inputs = $module.find('.js-file-check');

                $inputs.each(function(){
                    var $cur_input = $(this);

                    if($cur_input.val().indexOf(name) > 0){
                        if(!$cur_input.hasClass('js-file-start')){
                            $cur_input.remove();
                        }
                        else{
                            var $input_block = $module.find('.js-file-input-list');

                            $input_block.append($cur_input.clone(true).removeClass('js-file-start').val(''));
                            $cur_input.replaceWith($cur_input.clone(true).val(''));

                        }
                    }
                });

                $item.addClass('_remove');
                $item.remove();
                // setTimeout(function(){
                //     $item.remove();
                //
                //     var $items = $module.find('.object-fit-wrap');
                //
                //     if(!$items.length){
                //         var $inputs = $module.find('.js-file-check:not(.js-file-start)');
                //
                //         $inputs.remove();
                //         $module.removeClass('_onload');
                //     }
                //
                //     if($items.length < 6 && $module.hasClass('_full')){
                //         $module.removeClass('_full');
                //     }
                // },300);
            });
        },

        btn_reset_init:function(){
            this.$btn_form_reset.click(function(e){
                e.preventDefault();

                var $btn = $(this),
                    $form = $btn.closest('form');

                form_adjuster.form_reset($form);
            });


        },

        form_reset:function($form){
            var $inputs = $form.find('input:not([type=file]),textarea'),
                $file_modules = $form.find('.js-file-module'),
                $stars = $form.find('.js-star-item'),
                $select =  $form.find('.js-custom-select');

            $form.trigger('reset');
            $inputs.removeClass('_active active error valid');

            if($stars.length){
                var $star_input = $stars.find('.js-star-input');

                $star_input.removeAttr('checked');
                $stars.add($star_input).removeClass('_active');
            }

            $select.each(function(){
                var $select_item = $(this),
                    $options = $select_item.siblings('.js-select-inner').find('.js-option'),
                    $input = $select_item.siblings('.js-hidden-input'),
                    default_val = $options.eq(0).find('.text').text().trim(),
                    $placeholder = $select_item.children('span');

                $input.val(default_val);
                $placeholder.text(default_val);
                $options.not(':first-child').removeClass('_selected');
                $select_item.removeClass('_selected _active');
            });

            $file_modules.each(function(){
                var $module = $(this),
                    $input = $module.find('.js-file-check'),
                    $input_base = $input.filter('.js-file-start'),
                    $privew_list = $module.find('.js-file-preview-list');

                $module.removeClass('_onload _full');
                $input.not($input_base).remove();
                $input_base.replaceWith($input_base.clone(true).val(''));
                $privew_list.html('');
            });
        },

        //send data without form (add in favorite etc.)
        init_ajax_without_form: function() {
            form_adjuster.$btn_without_form.click(function(e) {
                e.preventDefault();

                var json = JSON.parse($(this).attr('data-json'));

                form_adjuster.form_send(JSON.stringify(json.parameters), json.action);
            });
        },

        //send form without custom ajax
        init_btn_no_ajax: function() {
            form_adjuster.$noajax_btn.click(function(e) {
                e.preventDefault();

                var $form = $(this).closest('form');

                $form.submit();
            })
        },

        //possible enter only number in input
        number_only_init: function() {
            $(document).on('keydown','.js-number-only', function(event) {
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
                    (event.keyCode == 65 && event.ctrlKey === true) ||
                    (event.keyCode >= 35 && event.keyCode <= 39)) {
                    return;
                } else {
                    if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                    }
                }
            });
        },

        send_validation: function() {
            // $.validator.messages.required = '';

            jQuery.extend(jQuery.validator.messages, {
                equalTo: "Пароли не совпадают",
                minlength: "Минимум 6 символов"
            });

            this.$form.each(function() {
                $(this).validate({

                    errorPlacement: function(error, element) {
                        // error.remove();
                        if(element.hasClass('show-error')){
                            error.insertBefore(element);
                        }
                    },

                    submitHandler: function(form) {
                        form_adjuster.$form_cur = $(form);

                        if(!$(form).hasClass('js-custom-ajax')){
                            form_adjuster.formData_assembly(form);
                        }
                        else{
                            $(form).trigger('custom_ajax');
                        }
                    }
                })
            });
        }


    };
});
