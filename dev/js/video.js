'use strict'
jQuery(function($){

  var video_list = {
     init:function(){
         this.api();
         this.init_cache();
         this.video_create();
         this.video_destroy();
         //this.load_preview();
     },

     init_cache:function(){
         this.$item      = $('.js-video-list-item');
         this.$btn       = $('.js-video-link');
         this.$overlay   = $('.js-overlay');
         this.$btn_close = $('.js-close-popup');
         this.$prev      = $('.js-video-prev');
         this.player;
     },



     api:function(){
         var tag = document.createElement('script');
         tag.src = "https://www.youtube.com/iframe_api?enablejsapi=1";
         var firstScriptTag = document.getElementsByTagName('script')[0];
         firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
     },

     video_init:function(id){
         var domen = window.location.hostname;

         video_list.player = new YT.Player('js-video-wrap', {
             height: '480',
             width: '720',
             videoId: id,
             playerVars: {
                 'origin': domen,
                 autoplay: 1
             }
         });
     },

     load_preview:function(){
         video_list.$prev.each(function(){
             var $link = $(this),
                 id = video_list.$btn.attr('data-video'),
                 $img = $link.find('.js-object-fit');

             $img.attr('src', 'https://img.youtube.com/vi/' + id + '/0.jpg');
         })
     },


     video_create:function(){
         video_list.$btn.click(function(){
             var $btn = $(this),
                 id = $btn.attr('data-video');

             video_list.video_init(id);
         })
     },

     video_destroy:function(){

         video_list.$btn_close.click(function(){
             video_list.video_close();
         });
     },

     video_close:function(){
         var $popup_active = $('.js-popup._active');

         if($popup_active.hasClass('js-popup-video')){
             video_list.player.destroy();
         }
     }
   };

     $(document).ready(function() {
        video_list.init();
    });
})
