var $_ = {
  init: function() {
    this.initCache();
    this.mobileMenu();
    this.events();
    this.showMore();
    this.part();
    this.initAnimatedBlocks();
    this.anchor();
    this.initForms();
    this.initPhotobox();

  },
  initCache: function() {
    this.$photobox          = $('.js-photobox');
    this.$hamburger = $(".js-hamburger");
    this.$nav = $(".js-nav");
    this.$closeMenu = $(".js-close-menu");
    this.$showMoreWrap = $(".js-show-more");
    this.$showMoreBtn = $(".js-show-more-btn");
    this.$showMoreText = $(".js-show-more-text");
    this.$particles = $(".js-particles");

    this.$animationBlock = $(".js-animation-block");
    this.$animationOnLoad = $(".js-animation-block-load");

    this.$nav_item = $(".js-nav-item");
    this.$window_width = $(window).width() ;
  },

  initPhotobox:function () {

        $_.$photobox.photobox('.js-photo-btn');

  },

  anchor: function() {
    $_.$nav_item.on("click", function(e) {
      e.preventDefault();
      var id = $(this).attr("href"),
        top = $(id).offset().top - 60;

      // $_.$header.removeClass('_active')
      $("body,html")
        .stop()
        .animate({ scrollTop: top }, 1500);
    });
  },

  part: function() {

    var delay = 600,
        scrollTimer = null;


    function checkBlock($el) {
      var pageOffsetTop = $(document).scrollTop();
          pageOffsetBottom = pageOffsetTop + window.innerHeight;


        var
          blockOffsetTop = $el.offset().top,
          blockOffsetBottom = blockOffsetTop + $el.height();

        if (blockOffsetBottom < pageOffsetTop) {
          return false
        } else if (blockOffsetTop > pageOffsetBottom) {
          return false
        } else if (blockOffsetTop < pageOffsetBottom) {
          return true
        }

    }

  function addId($el, index) {

      var block_id = "part-" + index;

      if (!$el.attr("id")) {
          $el.attr("id", block_id);
      }
  }

  function initParticles($el) {
      var
          block_id = $el.attr("id")
          mod = $el.data("mod")
          ? "../json/particles_gold.json"
          : "../json/particles.json";

      if (block_id && checkBlock($el) && !$el.hasClass('_active')) {
          $el.addClass('_active');
          particlesJS.load(block_id, mod, function() {});
      }
  }

    function stopAnimation($el) {
      for (var i = 0; i < pJSDom.length; i++) {
          if(pJSDom[i].pJS.canvas.parent == $el.attr('id') && !checkBlock($el)) {
            pJSDom[i].pJS.particles.move.enable = false;
            $el.removeClass('_active');
          }
      }
    }

    function refreshAnimation($el) {
      for (var i = 0; i < pJSDom.length; i++) {
        if(pJSDom[i].pJS.canvas.parent == $el.attr("id") && checkBlock($el) && !$el.hasClass('_active')) {
          pJSDom[i].pJS.particles.move.enable = true;
          pJSDom[i].pJS.fn.particlesRefresh();
          $el.addClass('_active');
        }
      }
    }

    function mainFn(){
      $_.$particles.each(function(index, el) {
        var $el = $(this);
        addId($el, index);
        initParticles($el);
        stopAnimation($el);
        refreshAnimation($el)
      })
    }

  function switchAll() {
    clearTimeout(scrollTimer);
    scrollTimer = setTimeout(function() {
        mainFn();
    }, delay);
  }

  switchAll();

  $(window).scroll(function() {
    switchAll()
  });
    //background: linear-gradient(90deg, #00BFD6 0%, #0E78CD 10.2%, #FFFFFF 21.18%, #3DC3CC 38.82%, #AE87D6 65.1%, #1D31C4 100%);
  },

  initAnimatedBlocks: function() {
    var $self = this,
      stopScrollingDelay = 600,
      whileScrollingDelay = 600,
      checkIsReady = true,
      scrollTimer = null,
      pageOffsetTop = null,
      pageOffsetBottom = null;

    switchAnimation();
    $_.$animationOnLoad.addClass("_animate");

    $(window).scroll(function() {
      switchAnimation();
    });

    function switchAnimation() {
      clearTimeout(scrollTimer);
      scrollTimer = setTimeout(function() {
        checkAnimationBlocks();

      }, stopScrollingDelay);

      if (checkIsReady) {
        checkIsReady = false;
        checkAnimationBlocks();

        setTimeout(function() {
          checkIsReady = true;
        }, whileScrollingDelay);
      }
    }

    function checkAnimationBlocks() {
      pageOffsetTop = $(document).scrollTop();
      pageOffsetBottom = pageOffsetTop + window.innerHeight;

      $self.$animationBlock.each(function() {
        var $el = $(this),
          blockOffsetTop = $el.offset().top,
          blockOffsetBottom = blockOffsetTop + $el.height();

        if (blockOffsetBottom < pageOffsetTop) {
          // $el.removeClass('_animate');
        } else if (blockOffsetTop > pageOffsetBottom) {
          //$el.removeClass('_animate');
        } else if (blockOffsetTop < pageOffsetBottom) {
          $el.addClass("_animate");
        }
      });
    }
  },

  showMore: function() {
    $_.$showMoreBtn.click(function() {
      $(this).toggleClass("_active");
      $(this)
        .parent()
        .find($_.$showMoreWrap)
        .slideToggle();
      if ($(this).hasClass("_active")) {
        $_.$showMoreText.text("Свернуть");
      } else {
        $_.$showMoreText.text("Показать еще");
      }
    });
  },

  mobileMenu: function() {
    $_.$hamburger.click(function() {
      $_.$nav.toggleClass("_active");
    });

    $_.$closeMenu.click(function() {
      $_.$nav.removeClass("_active");
    });

    $(window).scroll(function() {
      $_.$nav.removeClass("_active");
    });
  },

  events: function() {
    $("body").on("click", function(e) {
      var $el = $(e.target);

      if (
        !$el.hasClass("js-body-click-close") &&
        !$el.closest(".js-body-click-close").length
      ) {
        var $elements = $(".js-body-click-close");

        $.each($elements, function(key, item) {
          var $item = $(item);
          if (!($item.is("input") && $item.val()))
            $item.add(".js-filter-overlay").removeClass("_active");
        });
      }
    });

    $(".js-ps").perfectScrollbar();
  },
  initForms: function() {
    form_adjuster.init({
      file: false,
      success: function() {
        var $form = $(form_adjuster.$form_cur),
          $inputs = $form.find("input,textarea"),
          $popups = $(".js-popup"),
          $popup_thx = $(".js-thx"),
          $popup_overlay = $(".js-overlay"),
          $localThx = $(".js-local-thx");

        $popups.removeClass("_active");
        if(!$form.hasClass('js-download')) {
          $popup_thx.add($popup_overlay).addClass("_active");
        }
        else if ($form.data('src')){
          var win = window.open($form.data('src'), '_blank');
          win.focus();
        }

        setTimeout(function() {
          $form.trigger("reset");
          $inputs.removeClass("valid error active _active");
          if($form.hasClass('js-download')){
            console.log('download')
          }
        }, 500);

        setTimeout(function() {
          if ($popup_thx.hasClass("_active") || $localThx.hasClass("_active") ) {
            $popup_thx
              .add($popup_overlay)
              .add($localThx)
              .removeClass("_active");
          }
        }, 4000);
      }
    });
  }
};

$(document).ready(function() {
  $_.init();
});
