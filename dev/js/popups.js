'use strict'
jQuery(function($){

    var popup = {
        init:function(){
            this.init_cache();
            this.events();
        },

        init_cache:function(){
            this.$overlay   = $('.js-overlay');
            this.$btn_close = $('.js-close-popup');
            this.$popup     = $('.js-popup');
            this.$btn       = $('.js-open-popup');
        },

        events:function(){


            popup.$btn.click(function (e) {
                e.preventDefault();
                var $popupName  = $(this).data('popup'),
                    $popup      = popup.$popup.filter('[data-id = ' + $popupName + ']');

                $popup.add(popup.$overlay).addClass('_active');
            });

            popup.$btn_close.click(function () {
                popup.close_popup();
            });

        },

        close_popup:function(){
            var $popup_active = $('.js-popup._active');

            popup.$overlay.removeClass('_active');
            $popup_active.removeClass('_active');
        }

    };

    $(window).load(function(){
        popup.init();
    });
});